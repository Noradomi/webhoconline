// Modules cai dat
const express = require('express');
const exphbs = require('express-handlebars');
const express_handlebars_sections = require('express-handlebars-sections');
const session = require('express-session');
const Passport = require('passport');
const flash = require('connect-flash');
const validator = require('express-validator');

// Modules tu viet: Routers
const authRoute = require('./app/routes/auth.route');
const productRoute = require('./app/routes/product.route');
const accountRoute = require('./app/routes/account.route');

const port = 3000;
const app = express();

// Config cho PassportJS
// require('./app/configs/passport')(Passport);

var env = require('dotenv').config();

// View template engine
const handlebarsHelpers = require('handlebars-helpers');
const helpers =  handlebarsHelpers();
app.engine(
	'.hbs',
	exphbs({
		defaultLayout: 'main',
		extname: '.hbs',
		section: express_handlebars_sections(),
		helpers: {
			section: function(name, options) {
				if (!this._sections) {
					this._sections = {};
				}
				this._sections[name] = options.fn(this);
				return null;
			},
			getStatusMsg: function(status) {
			    if (status == 0)
			        return "Chưa duyệt.";
			    else
			        return "Đã duyệt.";
			},
			getStatusBlockMsg: function(status) {
			    if (status ==0 )
			        return "Duyệt";
			    else
			        return "Đã duyệt";
			},
			getStatusColor: function(status) {
			    if (status == 0)
			    	return "#D44638";
			    else
			        return "#3c763d";
			},
			getStatusBlockBtnClass: function(status) {
			    if (status == 0)
			        return "btn-danger";
			    else
			        return "btn-success";
			},
			compare: helpers.compare,
			generatePagination: function(route, page, count) {
			    let pageStr = "";
			    const pageMax = 5;
			    let i = (page > pageMax)? page - (pageMax - 1): 1;
			    if (i !== 1)
			        pageStr += `<li class="page-item disabled"><a class="page-link" href="">...</a></li>`;
			    for (; i <= page +  pageMax - 1 && i <= count; i++){
			       if (i === page)
			           pageStr += `<li class="page-item active"><a class="page-link" href="#">${i}</a></li>`;
			       else
			           pageStr += `<li class="page-item"><a class="page-link" href="/${route}?page=${i}">${i}</a></li>`;
			       if (i === page + pageMax - 1 && i < count)
			           pageStr += `<li class="page-item disabled"><a class="page-link" href="">...</a></li>`;
			    }
			    return pageStr;
			},
			inc: function(value, options)
			{
			    return parseInt(value) + 1;
			},
			dec: function(value, options)
			{
			    return parseInt(value) - 1;
			},
			numberWithCommas: function(x) {
			    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			},
			parseDate: function(dateString, seperator) {
			    seperator = seperator || '-';
			    const date = new Date(dateString);
			    return [("0" + date.getDate()).slice(-2), ("0" + (date.getMonth() + 1)).slice(-2), date.getFullYear()].join(seperator);
			}
		}
	})
);
app.set('view engine', '.hbs');
app.set('views', './app/views');

// Sử dụng static resource
app.use(express.static('public'));

// Config cho req.body
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Session
app.use(
	session({
		cookie: { maxAge: 60000 * 30 * 24 },
		secret: 'finalproject',
		resave: true,
		saveUninitialized: true
	})
);

// Init PassportJS
app.use(Passport.initialize());
app.use(Passport.session());

// Dùng cho validate và trả về lỗi
app.use(validator());
app.use(flash());

//Models
var models = require('./app/models');

var Product = models.product;
var Category = models.category;

// Config các biến toàn cục 
app.use((req, res, next) =>
{
	res.locals.user = req.user || [{isloggedin: false}];
	//Get all categories for displaying
	(async() => {
		const Cat = await models.the_loai.categoriesAndChild();
		res.locals.Cat = Cat;
		next();
	})();
}
);

//  Routes
app.get('/', require('./app/controllers/homepage.controller'));
app.use('/auth', authRoute);
app.use('/product', productRoute);
app.use('/bidders', require('./app/routes/bidder.route'));
app.use('/seller', require('./app/routes/seller.route'));
app.use('/account', accountRoute);
app.use('/admin', require('./app/routes/admin.route'));

//load passport strategies
require('./app/configs/passport.js')(Passport, models.user);

//Sync Database
models.sequelize
	.sync()
	.then(function() {
		console.log('Nice! Database looks fine');
	})
	.catch(function(err) {
		console.log(err, 'Something went wrong with the Database Update!');
	});

// app.use(function(req, res, next) {
// 	return res.status(404).send({ message: 'Route' + req.url + ' Not found.' });
// });
require('./app/middleware/error.middleware')(app);
app.listen(port, () => console.log(`Server listen on port ${port}!`));

const hbs = require('hbs');


hbs.registerHelper("numberizeBoolean", function(bool) {
    return (bool)? 1:0;
});
hbs.registerHelper("getStatusMsg", function(status) {
    if (status == 0)
        return "Hoạt động";
    else
        return "Khóa";
});
hbs.registerHelper("getStatusColor", function(status) {
    if (status)
        return "#3c763d";
    else
        return "#D44638";
});
hbs.registerHelper("getStatusBlockMsg", function(status) {
    if (status)
        return "Khóa TK";
    else
        return "Mở khóa";
});
hbs.registerHelper("getStatusBlockBtnClass", function(status) {
    if (status)
        return "btn-danger";
    else
        return "btn-success";
});