-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: webhoconline
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `khoa_hocs`
--

DROP TABLE IF EXISTS `khoa_hocs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `khoa_hocs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ngay_dang` datetime DEFAULT NULL,
  `ten_khoa_hoc` varchar(255) DEFAULT NULL,
  `gia` int(11) DEFAULT NULL,
  `mo_ta` text,
  `gia_khuyen_mai` int(11) DEFAULT NULL,
  `so_luot_like` int(11) DEFAULT '0',
  `so_luot_dislike` int(11) DEFAULT '0',
  `img_url` varchar(255) DEFAULT NULL,
  `is_validate` enum('0','1') DEFAULT '0',
  `status` enum('active','inactive') DEFAULT 'active',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `nguoi_day_id` int(11) DEFAULT NULL,
  `the_loai_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nguoi_day_id` (`nguoi_day_id`),
  KEY `the_loai_id` (`the_loai_id`),
  FULLTEXT KEY `text_idx` (`ten_khoa_hoc`),
  CONSTRAINT `khoa_hocs_ibfk_1` FOREIGN KEY (`nguoi_day_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `khoa_hocs_ibfk_2` FOREIGN KEY (`the_loai_id`) REFERENCES `the_loai_cons` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-04 20:30:43
