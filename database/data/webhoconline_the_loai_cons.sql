-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: webhoconline
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `the_loai_cons`
--

LOCK TABLES `the_loai_cons` WRITE;
/*!40000 ALTER TABLE `the_loai_cons` DISABLE KEYS */;
INSERT INTO `the_loai_cons` VALUES (1,'Lập trình Web','2020-01-05 09:39:59','2020-01-05 09:39:59',1),(2,'Lập trình Mobile','2020-01-05 09:39:59','2020-01-05 09:39:59',1),(3,'Ngôn ngữ lập trình','2020-01-05 09:39:59','2020-01-05 09:39:59',1),(4,'CSDL','2020-01-05 09:39:59','2020-01-05 09:39:59',1),(5,'Lập trình Game','2020-01-05 09:39:59','2020-01-05 09:39:59',1),(6,'Tài chính','2020-01-05 09:39:59','2020-01-05 09:39:59',2),(7,'Giao tiếp','2020-01-05 09:39:59','2020-01-05 09:39:59',2),(8,'Quản lý','2020-01-05 09:39:59','2020-01-05 09:39:59',2),(9,'Chiến lược','2020-01-05 09:39:59','2020-01-05 09:39:59',2),(10,'Sales','2020-01-05 09:39:59','2020-01-05 09:39:59',2),(11,'Leadership','2020-01-05 09:51:11','2020-01-05 09:51:11',3),(12,'Quản lý tài chính cá nhân','2020-01-05 09:51:11','2020-01-05 09:51:11',3),(13,'Phát triển sự nghiệp','2020-01-05 09:51:11','2020-01-05 09:51:11',3),(14,'Hạnh phúc','2020-01-05 09:51:11','2020-01-05 09:51:11',3),(15,'Tôn giáo và tâm linh','2020-01-05 09:51:11','2020-01-05 09:51:11',3),(16,'Thiết kế Web','2020-01-05 09:51:11','2020-01-05 09:51:11',4),(17,'Thiết kế Đồ họa','2020-01-05 09:51:11','2020-01-05 09:51:11',4),(18,'Công cụ thiết kế','2020-01-05 09:51:11','2020-01-05 09:51:11',4),(19,'3D & Hoạt họa','2020-01-05 09:51:11','2020-01-05 09:51:11',4),(20,'Thiết kế Thời trang','2020-01-05 09:51:11','2020-01-05 09:51:11',4),(21,'Sức khỏe thể chất','2020-01-05 09:58:35','2020-01-05 09:58:35',5),(22,'Sức khỏe tinh thần','2020-01-05 09:58:35','2020-01-05 09:58:35',5),(23,'Yoga','2020-01-05 09:58:35','2020-01-05 09:58:35',5),(24,'Tự vệ','2020-01-05 09:58:35','2020-01-05 09:58:35',5),(25,'Nhạc cụ','2020-01-05 09:58:35','2020-01-05 09:58:35',6),(26,'Nhạc lý','2020-01-05 09:58:35','2020-01-05 09:58:35',6),(27,'Sản xuất âm nhạc','2020-01-05 09:58:35','2020-01-05 09:58:35',6),(28,'Thanh nhạc','2020-01-05 09:58:35','2020-01-05 09:58:35',6),(29,'Khoa học tự nhiên','2020-01-05 10:01:14','2020-01-05 10:01:14',NULL);
/*!40000 ALTER TABLE `the_loai_cons` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-05 19:24:39
