const models = require('../models');

module.exports = async (req, res) => {
	Pro2 = await models.khoa_hoc.findAll();

	// Pro4 = await models.product.top5BiddedProducts();

	// Pro6 = await models.product.top5PricingProducts();

	Cat = await models.the_loai.categoriesAndChild();

	PTNotParent = await models.product_type.findAllProductTypeNotParent();

	console.log(Pro2.length);
	if (req.isAuthenticated()) {
		req.user.isloggedin = true;
		res.render('web/homepage', {
			user: [req.user],
			isBidder: req.user.role === 0,
			isSeller: req.user.role === 1,
			Pro2: Pro2,
			Cat: Cat,
			PTNotParent: PTNotParent
		});
	} else {
		let user = [
			{
				isloggedin: false
			}
		];
		res.render('web/homepage', {
			user: user,
			Pro2: Pro2,
			Cat: Cat,
			PTNotParent: PTNotParent
		});
	}
};
