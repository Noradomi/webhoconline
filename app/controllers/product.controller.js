const db = require('../models');
var createError = require('http-errors');
var Product = db.product;
var Category = db.category;
var bid_details = db.bid_details;

module.exports.search = async (req, res) => {
	// Lấy query từ req.
	const query = req.query.q;
	const ptId = req.query.selcat;

	console.log('Ket qua query : ' + req.query.selsort);

	let results = await db.khoa_hoc.searchAllByFTS(query, ptId);

	Cat = await db.the_loai.categoriesAndChild();

	PTNotParent = await db.product_type.findAllProductTypeNotParent();
	if (req.isAuthenticated()) {
		req.user.isloggedin = true;
		res.render('web/searproduct', {
			user: [req.user],
			isBidder: req.user.role === 0,
			isSeller: req.user.role === 1,
			pros: results,
			query: query,
			countPros: results.length,
			Cat: Cat,
			PTNotParent: PTNotParent
		});
	} else {
		let user = [
			{
				isloggedin: false
			}
		];
		res.render('web/searproduct', {
			user: user,
			pros: results,
			query: query,
			countPros: results.length,
			Cat: Cat,
			PTNotParent: PTNotParent
		});
	}
};
// PRODUCT DETAIL
module.exports.productdetail = async (req, res, next) => {
	try {
		const productID = req.query.ID;
		
		if(!parseInt(productID)){
			res.redirect(req.get('referrer'));
			return;
		}

		const product = await db.khoa_hoc.findByID(productID);
		
		//Product lỗi
		if(!product) {
			const error = {status : 404, message: "Khóa học không tồn tại"};
			return next(createError(404, error));
		}

		//Tìm thể loại
		const category = await db.the_loai_con.findByID(product.the_loai_id);

		//Product có thể loại
		let fatherCategory = null;
		if(category && category.the_loai_cha_id) {
			fatherCategory = await db.the_loai.findByID(category.the_loai_cha_id);
		}

		//Lấy thông tin giáo viên
		const teacher = await db.user.findById(product.nguoi_day_id);

		//Lấy thông tin bài học
		let lessons = await db.khoa_hoc.findAllLessons(productID);
		//Tạo thông tin khóa học
		let courseLength = 0;
		for(let x of lessons) {
			courseLength += x.thoi_luong;
		}
		const courseInfo = {lessonsCount: lessons.length, courseLength };
		//Set null để có hiển thị hợp lý
		if(lessons && lessons.length === 0) {
			lessons = null;
		}

		res.render('web/productdetail.hbs', {product, teacher, category, fatherCategory, lessons, courseInfo});
		
	} catch (error) {
		return next(createError(500, error));
	}
};
