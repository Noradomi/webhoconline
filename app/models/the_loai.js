module.exports = function(sequelize, Sequelize) {
	var the_loai = sequelize.define('the_loai', {
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: Sequelize.INTEGER
		},

		ten_the_loai: {
			type: Sequelize.TEXT
		},

		status: {
			type: Sequelize.ENUM('active', 'inactive'),
			defaultValue: 'active'
		}
	});

	the_loai.categoriesAndChild = async function() {
		let res = await the_loai.findAll();
		res.forEach(c => {
			c.hasChildCates = false;
			c.getTheLoaiCon().then(childCates => {
				if (childCates.length !== 0) {
					c.hasChildCates = true;
					c.childCates = childCates;
				}
			});
		});
		return res;
	};

	the_loai.findByID = async (_id) => {
		return await the_loai.findByPk(_id);
	}

	return the_loai;
};
