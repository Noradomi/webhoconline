const fs = require("fs");

module.exports = function(sequelize, Sequelize) {
	var bai_hoc = sequelize.define("bai_hoc", {
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: Sequelize.INTEGER
		},

		ten_bai_hoc: {
			type: Sequelize.STRING
		},

		video_url: {
			type: Sequelize.STRING
		},
		thoi_luong: {
			type: Sequelize.STRING
		},

		status: {
			type: Sequelize.ENUM("active", "inactive"),
			defaultValue: "active"
		}
	});

	bai_hoc.createFromArr = async function(files, proId, ten, thoiluong) {
		{
			let videoName;
			let i = 1;
			for (const f of files) {
				videoName = proId + "-" + i;
				await bai_hoc.create({
					khoa_hoc_id: proId,
					ten_bai_hoc: ten[i - 1] || `Bài ${i}`,
					video_url: `${videoName}.mp4`,
					thoi_luong: thoiluong[i - 1] || null
				});
				fs.rename(
					`./public/img/khoahoc/${f.filename}`,
					`./public/img/khoahoc/${videoName}.mp4`,
					function(err) {
						if (err) console.log("ERROR: " + err);
					}
				);
				++i;
			}
		}
	};

	return bai_hoc;
};
