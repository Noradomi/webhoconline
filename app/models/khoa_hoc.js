const fs = require('fs');

module.exports = function(sequelize, Sequelize) {
	var khoa_hoc = sequelize.define(
		'khoa_hoc',
		{
			id: {
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER
			},

			ngay_dang: {
				type: Sequelize.DATE,
				defaultValue: Sequelize.NOW
			},

			ten_khoa_hoc: {
				type: Sequelize.STRING,
				notEmpty: true
			},

			gia: {
				// giá khởi điểm
				type: Sequelize.INTEGER,
				notEmpty: true
			},

			mo_ta: {
				type: Sequelize.TEXT
			},

			gia_khuyen_mai: {
				// giá mua ngay
				type: Sequelize.INTEGER
			},

			so_luot_like: {
				type: Sequelize.INTEGER,
				defaultValue: 0
			},
			so_luot_dislike: {
				type: Sequelize.INTEGER,
				defaultValue: 0
			},

			img_url: {
				type: Sequelize.STRING,
				notEmpty: true
			},

			is_validate: {
				// đã được kiểm duyệt chưa
				type: Sequelize.ENUM('0', '1'),
				defaultValue: '0'
			},

			status: {
				type: Sequelize.ENUM('active', 'inactive'),
				defaultValue: 'active'
			}
		},
		{
			indexes: [
				// add a FULLTEXT index
				{ type: 'FULLTEXT', name: 'text_idx', fields: ['ten_khoa_hoc'] }
			]
		}
	);

	khoa_hoc.createFromArr = async function(files, proId) {
		// console.log(files);
		files.forEach(async f => {
			console.log(f);
			fs.rename(
				`./public/img/khoahoc/${f.filename}`,
				`./public/img/khoahoc/${proId}.jpg`,
				function(err) {
					if (err) console.log('ERROR: ' + err);
				}
			);
		});
	};

	khoa_hoc.searchAllByFTS = async function(query, ptId) {
		let sql;
		if (query != '') {
			sql = `SELECT p.* FROM khoa_hocs p  WHERE MATCH(ten_khoa_hoc) AGAINST ('${query}' IN NATURAL LANGUAGE MODE)`;
			if (parseInt(ptId) !== 0) {
				sql += ` AND the_loai_id = ${ptId}`;
			}
		} else {
			sql = `SELECT p.* FROM khoa_hocs p WHERE p.the_loai_id = ${ptId}`;
		}

		let res = await sequelize.query(sql, {
			type: sequelize.QueryTypes.SELECT
		});

		return res;
	};

	khoa_hoc.getAllCourses = function(page) {
		return khoa_hoc.findAll({
			offset:((page-1)*15),
			limit : 15
		});
	};
	khoa_hoc.getCountAll = function() {
		return khoa_hoc.count();
	};
	khoa_hoc.setValidate = async function(id, isValidate) {
		let sql = `update khoa_hocs set is_validate = '${isValidate}' where id = ${id}`;
		console.log(sql);
		await sequelize.query(sql, {
			type: sequelize.QueryTypes.UPDATE
		});
	};

	khoa_hoc.delete = async (id) => {
		let sql = `DELETE FROM khoa_hocs WHERE id = ${id}`;
		await sequelize.query(sql, {
			type: sequelize.QueryTypes.DELETE
		});
	};

	khoa_hoc.findByID = async (_id) => {
		return await khoa_hoc.findByPk(_id);
	}

	khoa_hoc.findAllLessons = async (_id) => {
		let sql = `SELECT p.* FROM bai_hocs p WHERE p.khoa_hoc_id = ${_id}`;
		
		let res = await sequelize.query(sql, {
			type: sequelize.QueryTypes.SELECT
		});

		return res;
	}


	return khoa_hoc;
};
