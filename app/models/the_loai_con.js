module.exports = function(sequelize, Sequelize) {
	var the_loai_con = sequelize.define("the_loai_con", {
		id: {
			autoIncrement: true,
			primaryKey: true,
			type: Sequelize.INTEGER
		},

		ten: {
			type: Sequelize.STRING
		}
	});

	the_loai_con.findAllProT = function() {
		return the_loai_con.findAll();
	};

	the_loai_con.findByID = async (_id) => {
		return await the_loai_con.findByPk(_id);
	}
	return the_loai_con;
};
