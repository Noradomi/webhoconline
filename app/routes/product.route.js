const express = require('express');
const router = express.Router();

const productController = require('../controllers/product.controller');

router.get('/search', productController.search);
router.get('/detail', productController.productdetail);

module.exports = router;