$("#input-drop1").fileinput({
	browseClass: "btn btn-primary btn-block",
	showCaption: false,
	showRemove: false,
	showUpload: false
});

$("#input-drop2").fileinput({
	browseClass: "btn btn-primary btn-block",
	showCaption: false,
	showRemove: false,
	showUpload: false
});

$("#datetimepicker").datetimepicker({
	format: "YYYY-MM-DDTHH:mm:ssZ"
});

$(document).ready(function() {
	var counter = 0;

	$("#addrow").on("click", function() {
		var newRow = $("<tr>");
		var cols = "";

		cols +=
			'<td><input type="text" class="form-control" name="ten_bai_hoc[]"' +
			counter +
			'"/></td>';
		cols +=
			'<td><input type="text" class="form-control" name="thoi_luong[]"' +
			counter +
			'"/></td>';
		counter + '"/></td>';

		cols +=
			'<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Xóa"></td>';
		newRow.append(cols);
		$("table.order-list").append(newRow);
		counter++;
	});

	$("table.order-list").on("click", ".ibtnDel", function(event) {
		$(this)
			.closest("tr")
			.remove();
		counter -= 1;
	});
});

function calculateRow(row) {
	var price = +row.find('input[name^="ten_bai_hoc"]').val();
}

function calculateGrandTotal() {
	var grandTotal = 0;
	$("table.order-list")
		.find('input[name^="ten_bai_hoc"]')
		.each(function() {
			grandTotal += +$(this).val();
		});
	$("#grandtotal").text(grandTotal.toFixed(2));
}
