$( document ).ready(function() {

    const blockMsg = ["Duyệt", "Hủy duyệt"];
    const statusMsg = ["Đã duyệt", "Duyệt"];
    const statusMsgColor = ["#D44638", "#3c763d"];
    const urlImageSeperator = ", ";

    $('.set-status-btn').on('click', async (e) => {
        const $this = $(e.target);
        const id = $this.attr('data-id');
        const isValidate = parseInt($this.attr('data-isValidate'));
        Alert.warning("Bạn có chắc muốn " + blockMsg[isValidate] + " khóa học này không")
            .then(async (result) => {
                if (result.value){
                    
                    const result = await setUserStatusDB(id, isValidate ^ 1);
                    if (result.isSuccess){
                        Alert.success(blockMsg[isValidate] + " khóa học thành công !!!");
                        setStatusBtnStyle($this, id, isValidate ^ 1);
                    } else {
                        Alert.error(blockMsg[isValidate] + "khóa học thất bại !!!");
                    }
                }
            })
    })
    function setUserStatusDB(id, isValidate){
        showLoading();
        return new Promise((resolve, reject) => {
            $.ajax({
                url: "/admin/user/setValidate",
                type: "POST",
                data: {
                    id: id,
                    isValidate: isValidate
                },
                success: (result) => {
                    resolve(result);
                },
                error: (err) => {
                    reject(err);
                },
                complete: () => {
                    hideLoading();
                }

            })
        })
    }
    function setStatusBtnStyle($button, id, isValidate){
        if (isValidate){
            $button.addClass("btn-success");
            $button.removeClass("btn-danger");
        } else {
            $button.removeClass("btn-success");
            $button.addClass("btn-danger");
        }
        $button.text(statusMsg[isValidate ^ 1]);
        $button.attr("data-isValidate", isValidate);
        const $statusText = $(`.status-text[data-id=${id}]`);
        $statusText.text(statusMsg[isValidate]);
        $statusText.css('color', statusMsgColor[isValidate]);
    }

    
});
